console.log("The bot is starting.");

// Import required packages
var twit = require('twit');
var access = require('./access');
var tweet = new twit(access);
var jsonfile = require('jsonfile');
var sw = require('stopword');

// Set filename to save data
var filename = 'english_crm.json';

// Set cityname data to be collected from, topic of data collection, and keywords to be searched
var cityname = 'nyc';
var topic = 'environment';
var arr = ['global warming','wildfire','climate change','hurricane florence','nature conservation','greenhouse gases','flood','forest','deforestation','ecofriendly','ozone','carbon dioxide','tree plantation','pollution'];

search_count = 0;

// Set up tweet stream
var stream = tweet.stream('statuses/filter', { track: arr, geocode: ['40.7128', '-74.0060', '50mi'], language:'en' })

// Start collecting data
stream.on('tweet', function (statuses) {
    //...
    var fs = require('fs');

    // Process data only if it's not truncated
    if(statuses.truncated==false)
	{
		var id = statuses.id_str;
        var handleid = statuses.user.id_str;
        var username = statuses.user.name;
        var handlename = statuses.user.screen_name;
        var profile = statuses.user.profile_image_url;
        var userloc = statuses.user.location;

		var date = statuses.created_at;

		var split_date = date.split(' ');

		var month = {
                        Jan: '01',
                        Feb: '02',
                        Mar: '03',
                        Apr: '04',
                        May: '05',
                        Jun: '06',
                        Jul: '07',
                        Aug: '08',
                        Sep: '09',
                        Oct: '10',
                        Nov: '11',
                        Dec: '12'
   	                };

		var time = {
                        0: '00',
                        1: '01',
                        2: '02',
                        3: '03',
                        4: '04',
                        5: '05',
                        6: '06',
                        7: '07',
                        8: '08',
                        9: '09'
		            };


		var geo = statuses.geo;
		var co_ordinates = [];
		if(geo == null)
		{
			co_ordinates = null;
		}
		else
		{
			var co_or = geo.coordinates;
			var co_ordinates = (co_or[0].toString()+', '+co_or[1].toString());
		}

        // As the data will be processed by Apache Solr, convert the date to Solr's date format yyyy-mm-ddThh:mm:ssZ
		var str_date = (split_date[5]+'-'+month[split_date[1]]+'-'+split_date[2]);

		var split_time = split_date[3].split(':');

		split_time[2]='00';

		if(parseInt(split_time[1],10)>29)
		{
			var num = parseInt(split_time[0],10) + 1;
			if(num == 24)
			{
				split_time[0]='00';

			}
			else
			{
				if(num <10)
				{
					split_time[0]= time[num];
				}
			}

		}

		split_time[1]='00';
		var str_time = (split_time[0]+':'+split_time[1]+':'+split_time[2]);
        var final_datetime = (str_date+'T'+str_time+'Z');

        var input = statuses.text;

		input = input.replace(/\r?\n|\r/g,' ');
		input = input.replace(/&amp;/g,"&");
		
        // find urls
		var urls = input.match(/\bhttps?:\/\/\S+/gi);
		if(urls != null)
		{
			var url_num = urls.length;
		}

        // remove urls
		input = input.replace(/\bhttps?:\/\/\S+/gi,"");

        //remove punctuations
		var final = input.replace(/[.,\/!'$\"%\^&\*;:—{}?=\-_`~()|]|['+']/g,""); 	
		final = final.replace(/\s{2,}/g," ");
		final = final.replace(/’‘/g,"");


        // find hashtags
        var hashtags = final.match(/\#\w\w+\s?/g);
  	    if(hashtags != null)
		{
			var ht_num = hashtags.length;
			for(var i=0;i < ht_num;i++)
			{
				hashtags[i]=hashtags[i].replace('#','');
			}

		}

        // remove hashtags
		var final_ht = final.replace(/\#\w\w+\s?/g,"");			

        // find user mentions
		var mentions = final.match(/\@\w\w+\s?/g);

	    if(mentions != null)
        {
            var um_num = mentions.length;
            for(var i=0;i < um_num;i++)
            {
                mentions[i]=mentions[i].replace('@','');
            }

        }

        // remove user mentions
        var final_um = final_ht.replace(/\@\w\w+\s?/g,"");		

        // find emojis and remove them
        var emojis = final_um.match(/(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|[\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|[\ud83c[\ude32-\ude3a]|[\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g, ''); //extract emojis
        var final_emoji = final_um.replace(/(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|[\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|[\ud83c[\ude32-\ude3a]|[\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g, ''); //extract emojis
        var words = final_emoji.split(' ');

        // remove stopwords
        var sw = require('stopword');
        var words = sw.removeStopwords(words);

        var len = words.length;

        if(emojis!=null)
        {
            var le = emojis.length;
        }

        var text_en=[];
        for(var i = 0; i < len; i++)
        {
            if(words[i]!='')
            {
                text_en.push(words[i]);
            }
        }

        var text = text_en[0];
        for(var i = 1 ; i < text_en.length; i++)
        {
            text = (text+' '+text_en[i]);
        }


        // save the tweet if its not a retweet
        if(text_en[0]!='RT')
        {
            var file1 = {}

                
            file1.topic = topic;
            file1.city = cityname;
            file1.tweet_text = input_trim;
            file1.tweet_id = id;
            file1.tweet_lang = 'en';
            file1.text_en = text;
            file1.tweet_urls = urls;
            file1.hashtags = hashtags;
            file1.mentions = mentions;
            file1.tweet_emoticons = emojis;
            file1.tweet_loc = co_ordinates;
            file1.tweet_date = final_datetime;
            file1.username = username;
            file1.handleid = handleid;
            file1.handlename = handlename;
            file1.profile = profile;
            file1.userloc = userloc;

            search_count = search_count+1;

            fs.appendFile(filename, JSON.stringify(file1)+',', function fi(err){
                // console.log('Tweets Collected:'+search_count+'\n');
            });

    

  	    }

        // Stop streaming after collecting 10000 tweets  
        if(search_count > 10000)
            {
                stream.stop();
            }

        }


        
    }
})
