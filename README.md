### TweetBot : Tweet Collection Bot
* This bot was used to collect tweets to build a tweet corpus to build a tweet search engine.
* Twitter stream API was used to collect tweets on a particular topic.
* Tweets on 5 different topics in 5 different languages from 5 different cities were collected for the corpus. 