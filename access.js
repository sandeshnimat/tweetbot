module.exports = {
  consumer_key:         'consumer key : copy yours',
  consumer_secret:      'consumer secret : copy yours',
  access_token:         'access token : copy yours',
  access_token_secret:  'access token secret : copy yours',
  timeout_ms:           60*10000000,  // optional HTTP request timeout to apply to all requests.
  strictSSL:            true,     // optional - requires SSL certificates to be valid.
};